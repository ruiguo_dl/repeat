#! /bin/bash
idchange() {
echo "change mhap id\n"
files=$(ls correction/1-overlapper/results/*.mhap)
if files=$(ls correction/1-overlapper/results/*.mhap)
	then
	echo $files
	else
	printf "no mhap files\n"
fi
grep "cvt" correction/1-overlapper/mhap.sh | cut -d " " -f4,5,7 | head -n -1 | sed s'/.$//' > cvtnumber

for i in $files
do
	echo $i
	line=$(basename $i .mhap | sed 's/^0*//')
	echo $line
	num1=$(awk -v i1=$line 'FNR==i1 {print $3-$2}' cvtnumber)
	num2=$(awk -v i1=$line 'FNR==i1 {print $1}' cvtnumber)
#	echo "num1=$num1"
#	echo "num2=$num2"
	awk -v n1=$num1 -v n2=$num2 '{print $1+n1-1,$2+n2-1,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$7-$6,$11-$10}' \
			 $i > ${i%.mhap}_new.mhap
done			  
}
