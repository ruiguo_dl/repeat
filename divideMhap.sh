#! /bin/bash
divideMhap() {
inputfile=$1
threshold=$2
echo $inputfile
echo $threshold
lines=$(wc -l $inputfile | cut -d " " -f1)
threshold1=$(expr $threshold / 2)
onemore=0
echo $lines
echo $threshold
echo $threshold1
#filename=$(basename $inputfile .mhap) 
#folder=$(dirname $inputfile)
#printf "folder=%s\n" $folder
if [ $lines -le $threshold ]
then
    mv $inputfile part1.mhap
	return
fi
if [ $(expr $lines % $threshold) -le $threshold1 ]
then
	filenums=$(expr $lines / $threshold)
else
	filenums=$(expr $lines / $threshold + 1)
	onemore=1
fi
i=1
#echo $inputfile
#echo $threshold
#echo $threshold1
printf "filenum = %d \n" $filenums
printf "onemore= %d \n" $onemore
printf "i=%d \n" $i
while [ $i -lt $((filenums-1)) ]
do
	from=$(expr $threshold "*" $((i-1)) + 1)
	to=$(expr $threshold "*" $i)
	echo $from
	echo $to
	sed -n ${from},${to}p $inputfile > part${i}.mhap
	i=$((i+1))
	echo $i
done


echo "finish up"
if [ $onemore -eq 0 ]
then
	from=$(expr $threshold "*" $((i-1)) + 1)
	to=$lines
	echo $from
	echo $to

	sed -n ${from},${to}p $inputfile >part${i}.mhap
else
	from=$(expr $threshold "*" $((i-1)) + 1)
	to=$(expr $threshold "*" $i)
	echo $from
	echo $to
	sed -n ${from},${to}p $inputfile > part${i}.mhap
	i=$((i+1))
	echo $i
	from=$(expr $to + 1)
	to=$lines
	echo $from
	echo $to
	sed -n ${from},${to}p $inputfile > part${i}.mhap
fi
}
