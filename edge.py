#!/usr/bin/python
import sys


def buildEdgeWithFrom(inputfile,outputfile,threshold):
    with open(inputfile,'r') as f1:
        with open(outputfile,'w') as f2:
            linenum = 0
            for line in f1:
                linenum += 1
                fields = line.split(" ")
                readid1 = fields[0]
                readid2 = fields[1]


                id1strand = fields[4]
                id1from = int(fields[5])
                id1to = fields[6]
                id1replength = int(fields[12])

                id2strand = fields[8]
                id2from = int(fields[9])
                id2to = fields[10]
                id2replength = int(fields[13])
                if id1replength < 100 or id2replength < 100 \
                        or abs(id1replength - id2replength) > threshold:

                    continue

                fromrange1 = id1from // threshold * threshold
                fromrange2 = id2from // threshold * threshold
                meanlength = (id1replength+id2replength)/2

                f2.write(readid1 + '_' + id1strand + '_' + str(fromrange1) + ' ' +
                         readid2 + '_' + id2strand + '_' + str(fromrange2) +
                         ' ' + str(meanlength) + ' ' + str(linenum) + '\n')

def buildEdge(inputfile,outputfile,threshold):
    with open(inputfile) as f1:
        with open(outputfile,'w') as f2:
            for line in f1:
                fields = line.split(" ")
                readid1 = fields[0]
                readid2 = fields[1]


                id1strand = fields[4]
                id1from = int(fields[5])
                id1to = fields[6]
                id1replength = int(fields[7])

                id2strand = fields[9]
                id2from = int(fields[10])
                id2to = fields[11]
                id2replength = int(fields[12])

                if id1replength < 100 or id2replength < \
                        100 or abs(id1replength - id2replength) > 100:
                    continue

                fromrange1 = id1from // threshold * threshold
                fromrange2 = id2from // threshold * threshold

                f2.write(readid1 + '_' + id1strand + '_' + str(fromrange1) + ' ' +
                         readid2 + '_' + id2strand + '_' + str(fromrange2) +
                         ' ' + str(id1replength) + '\n')




inputname = sys.argv[1]
outputname = sys.argv[2]
threshold = int(sys.argv[3]) #from threshold
#buildEdge(inputname,outputname,100)
buildEdgeWithFrom(inputname,outputname,threshold)
