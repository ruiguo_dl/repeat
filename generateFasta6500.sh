#! /bin/bash
writefasta() {
inputfile=$(ls *.line)
echo $inputfile
if [ ! -e result ]
then
	mkdir result
	cd result
else
	cd result
fi

if [ -e temp.bed ]
then
	rm temp.bed
fi
for file in $inputfile
do
	name=${file%.line}
	printf "name=%s\n" $name
	if [ -e ${name}.mhap ]
	then
		rm ${name}.mhap
	fi
	mhapfile=${name}.mhap
	sed -n $(cat ../${name}.line) ../../R6500/$mhapfile > ${name}.mhap &
done
wait

for file in $inputfile
do
	name=${file%.line}
	awk '{print "read" $1 "_0",$6,$7,0,0,"+"}' ${name}.mhap >> temp.bed
done
sort -u temp.bed >  new.bed 
faidx -b new.bed -l ../../R6500/all.fasta > new.fasta



}
#	blastn -query ${name}.fasta -subject /home/guorui/project/repeat/drorep.ref -outfmt "6 qseqid sseqid pident qlen slen length mismatch gapopen qstart qend sstart send" > ${name}.blast 2>> blast.err
