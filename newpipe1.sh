#! /bin/bash
SECONDS=0
DIR=$( cd $(dirname $0) ; pwd -P )
printf "script path = %s\n" $DIR
source ${DIR}/idchange.sh
source ${DIR}/combineMhap.sh
source ${DIR}/generateFasta6500.sh
source ${DIR}/evaluation1.sh
# set flog vars to empty
lines=65000000 genomeSize= range=250 file= temp="temp" lendiff=200 fromlen=250 drops=4 n1=3 n2=9 degree=10 commu_size=10 breaks=200 outputfile= 
while getopts f:s:n:l:t:b:q:w:d:x:c:g:m:z: opt
do
	case $opt in
		f)	file=$OPTARG
			;;
		s)	genomeSize=$OPTARG
			;;
		n)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))
			continue
			fi
			range=$OPTARG
			;;
		l)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			lines=$OPTARG
			;;
		t)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			temp=$OPTARG
			;;
		b)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			breaks=$OPTARG
			;;
		x)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			drops=$OPTARG
			;;
		q)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			n1=$OPTARG
			;;
		w)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			n2=$OPTARG
			;;
		d)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			degree=$OPTARG
			;;
		c)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			commu_size=$OPTARG
			;;
		g)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			lendiff=$OPTARG
			;;
		m)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			fromlen=$OPTARG
			;;
		z)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			outputfile=$OPTARG
	esac
done
shift $((OPTIND - 1))
mkdir $temp
printf "file=%s\n" $file
printf "genomeSize=%s\n" $genomeSize
printf "range=%s\n"	$range
printf "lines=%s\n" $lines
printf "temp=%s\n" ${temp}
printf "breaks=%d\n" ${breaks}
printf "n1=%d\n" $n1
printf "n2=%d\n" $n2
printf "degree=%d\n" $degree
printf "commu_size=%d\n" $commu_size
printf "drops=%d\n" $drops
printf "lendiff=%d\n" $lendiff
printf "fromlen=%d\n" $fromlen
printf "outputfile=%s\n" $outputfile
home=$(pwd)
printf "original place=%s\n" $home
parameters=n1${n1}n2${n2}dr${drops}
printf "parameters=%s\n" $parameters
#canu -correct -p "step1" maxThreads=8 -d "$temp" genomeSize="$genomeSize" saveOverlaps=T saveReadCorrections=T corOutCoverage=400 corMinCoverage=0 -pacbio-raw "$file"

cd $temp
#idchange
#mv correction/1-overlapper/results/*new.mhap ./
#cat correction/2-correction/correction_outputs/*.fasta > all.fasta
#rm -r correction
printf "test range, divide and combine parameter\n"
folderName=$parameters
if [ -e $folderName ]
then
	cd $folderName
else
	mkdir $folderName
	cd $folderName
fi

printf "combine mhap files with line limit %s\n" $lines
#combineMhap $lines
printf "make mhap file for R community input\n"
printf "current folder = %s\n" $(pwd)
#for mhapfile in part*.mhap
#do
#	filename=$(basename $mhapfile .mhap)
#	printf "working on file %s\n" $mhapfile
#	${DIR}/edge.py $mhapfile ${filename}_edge.mhap $range
#done
printf "find community in the graph\n"
Rscript --vanilla  ${DIR}/community1.R ./ $degree $commu_size $n1 $n2 $breaks $drops
printf "extract result fasta file\n"
printf "the extract fasta folder name is %s\n" $(pwd)
writefasta
#cd result
printf "the result folder name is %s\n" $(pwd)
newSize=$(du new.fasta | cut -f1)
grep ">" new.fasta | cut -c2- > allnames
printf "use ovl to find overlap\n"
canu -correct -p "step2" maxThreads=8 -d "temp" genomeSize="$newSize" saveOverlaps=T corOutCoverage=400 corMinCoverage=0 Overlapper=ovl minReadLength=100 minOverlapLength=20 stopAfter=overlapStore -pacbio-corrected new.fasta
if [ -e all.out ]
then
	rm all.out
fi
for i in temp/correction/1-overlapper/001/*.ovb.gz
do

	echo $i
	overlapConvert -G temp/correction/step2.gkpStore -coords ${i}  >> all.out ;
done

printf "filter overlap file to merge repeat\n"
python ${DIR}/edge_new.py all.out temp/correction/step2.ovlStore.per-read.log $fromlen $lendiff newedge
printf "use community detection to merge repeat\n"
Rscript --vanilla  ${DIR}/merge.R ./ $degree $commu_size $n1 $n2 $breaks $drops
awk '{print $1,$5,$6}' all.out > new.out
sed -n $(cat result) new.out > new1.out
cut -d " " -f1 new1.out >linenum
while read line; do sed -n ${line}p temp/correction/step2.gkpStore/readNames.txt | cut -f2 ; done < linenum > linenames
paste linenames <(cut -d " " -f2-3 new1.out) > result.bed

faidx -b result.bed new.fasta > part2.fasta
cut -f2 temp/correction/step2.gkpStore/readNames.txt > canunames
comm -23 <(sort allnames) <(sort canunames) > leftnames
cat leftnames | xargs -n 1 samtools faidx new.fasta > part1.fasta
cat part1.fasta part2.fasta > result.fasta
#mv result.fasta ${home}/
#rm $temp
evaluation && rm -r temp
duration=$SECONDS
echo $folderName >> ${home}/test.time
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed." >> ${home}/test.time



