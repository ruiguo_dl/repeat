#! /bin/bash
combineMhap () { 
threashold=$1
filenum=1
linesum=0
for i in *new.mhap
do
	printf "files=%s\n" $i
	lines=$(wc -l $i | cut -d " " -f1)
	printf "lines=%d\n" $lines
	printf "filelines = %d \n" $lines
	printf "linesum=%d\n" $(($linesum + $lines))
	if  [ $(($linesum + $lines)) -lt $threashold ]
	then
		if [ -f part${filenum}.mhap ]
		then
			cat $i >> part${filenum}.mhap
			((linesum+=$lines))
			printf "linesum=%d\n" $linesum
			printf "part %d\n" $filenum
		else
			touch part${filenum}.mhap
			cat $i >> part${filenum}.mhap
			linesum=$lines
			printf "linesum=%d\n" $linesum
			printf "part %d\n" $filenum
		fi
	else
		if [ -s part${filenum}.mhap ]
		then
			((filenum+=1))
		fi
		linesum=$lines
		cat $i >> part${filenum}.mhap
		printf "linesum=%d\n" $linesum
		printf "part %d\n" $filenum
	fi
done
}
