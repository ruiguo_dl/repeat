#! /home/guorui/anaconda2/bin/python
import sys
import pysam


fname = sys.argv[1]
#fname='result.sam'
samfile = pysam.AlignmentFile(fname)
matchlist = []
indellist = []
matchedbases = 0
indelbases = 0
totalbases = 0
readlength = 0

for read in samfile:
    if not read.is_unmapped:
        matched = 0
        insertion = 0
        deletion = 0
        cigar = read.cigartuples
        for pair in cigar:
            if pair[0] == 0:
                matched += pair[1]
            if pair[0] == 1:
                insertion += pair[1]
            if pair[0] == 2:
                deletion += pair[1]
        # if not read.is_secondary:
        #     readlength = float(read.query_length)
        readlength = float(read.inferred_length)
        # if matched/readlength > 1:
        #     ano.write(str(matched) + ' ' + str(readlength) + '\n')
        matchlist.append(matched / readlength)
        indellist.append((insertion + deletion) / readlength)
        totalbases += readlength
        matchedbases += matched
        indelbases += (insertion + deletion)

with open('analysis.out','w') as f:
    for i in range(len(matchlist)):
        f.write(str("{0:.4f}".format(matchlist[i])) + "\t" + str("{0:.4f}".format(indellist[i])) + "\n")
        sys.stdout.write("%f\t%f\n" % (matchlist[i],indellist[i]))
    f.write(str("{0:.4f}".format(matchedbases / float(totalbases))) + "\t" + str("{0:.4f}".format(indelbases /
                           float(totalbases))) + "\n")
    sys.stdout.write("%f\t%f\n" % (matchedbases / float(totalbases), indelbases /
                           float(totalbases)))
