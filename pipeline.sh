#! /bin/bash
SECONDS=0
DIR=$( cd $(dirname $0) ; pwd -P )
printf "script path = %s\n" $DIR
source ${DIR}/idchange.sh
source ${DIR}/combineMhap.sh
source ${DIR}/writefasta.sh
source ${DIR}/divideMhap.sh
source ${DIR}/evaluation.sh
# set flog vars to empty
lines=15000000 genomeSize= range=300 file= temp="temp" replen=100 fromlen=50 replendiff=100 depth=3 ratio=0.9 degree=50 commu_size=50 breaks=200 n1=3 n2=3 ovl=1 divide=1 outputfile= 
while getopts f:s:n:l:t:b:q:w:d:c:l:m:e:o:i:a:p:z: opt
do
	case $opt in
		f)	file=$OPTARG
			;;
		s)	genomeSize=$OPTARG
			;;
		n)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))
			continue
			fi
			range=$OPTARG
			;;
		l)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			lines=$OPTARG
			;;
		
		t)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			temp=$OPTARG
			;;
		b)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			breaks=$OPTARG
			;;
		q)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			n1=$OPTARG
			;;
		a)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			ratio=$OPTARG
			;;
		p)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			depth=$OPTARG
			;;
		w)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			n2=$OPTARG
			;;
		d)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			degree=$OPTARG
			;;
		c)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			commu_size=$OPTARG
			;;
		l)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			replen=$OPTARG
			;;
		m)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			fromlen=$OPTARG
			;;
		e)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			replendiff=$OPTARG
			;;
		o)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			ovl=$OPTARG
			;;
		i)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			divide=$OPTARG
			;;
		z)	if [[ $OPTARG = -* ]]; then
			((OPTIND--))  
			continue
			fi
			outputfile=$OPTARG
	esac
done
shift $((OPTIND - 1))
#rm -r $temp
#mkdir $temp
printf "file=%s\n" $file
printf "genomeSize=%s\n" $genomeSize
printf "range=%s\n"	$range
printf "lines=%s\n" $lines
printf "temp=%s\n" ${temp}
printf "breaks=%d\n" ${breaks}
printf "n1=%d\n" $n1
printf "n2=%d\n" $n2
printf "degree=%d\n" $degree
printf "commu_size=%d\n" $commu_size
printf "replen=%d\n" $replen
printf "fromlen=%d\n" $fromlen
printf "lengthdiff=%d\n" $replendiff
printf "ratio=%s\n" $ratio
printf "depth=%d\n" $depth
printf "ovl=%d\n" $ovl
printf "divide=%d\n" $divide
printf "outputfile=%s\n" $outputfile
home=$(pwd)
printf "original place=%s\n" $home
parameters=n${range}l${lines}b${breaks}n1${n1}n2${n2}deg${degree}c${commu_size}o${ovl}div${divide}
printf "parameters=%s\n" $parameters
#canu -correct -p "step1" -d "$temp" genomeSize="$genomeSize" saveOverlaps=T saveReadCorrections=T corOutCoverage=400 corMinCoverage=0 -pacbio-raw "$file" && cd $temp
cd $temp
#idchange
#rm ${temp}/*.mhap
#cp correction/1-overlapper/results/*new.mhap ./
#cat correction/2-correction/correction_outputs/*.fasta > all.fasta
#rm -r correction
printf "test range, divide and combine parameter\n"
folderName=$parameters
if [ -e $folderName ]
then
	cd $folderName
else
	mkdir $folderName
	cd $folderName
fi

if [ $divide -eq 0 ]
then
	printf "combine mhap files with line limit %s\n" $lines
	combineMhap $lines
else
	printf "divide mhap files with line limit %s\n" $lines
	#cat ../*new.mhap > all.mhap
	divideMhap ../all.mhap $lines
fi
printf "make mhap file for R community input\n"
printf "current folder = %s\n" $(pwd)
for mhapfile in part*.mhap
do
	filename=$(basename $mhapfile .mhap)
	printf "working on file %s\n" $mhapfile
	${DIR}/edge.py $mhapfile ${filename}_edge.mhap $range
done
Rscript --vanilla  ${DIR}/allCluster.R ./ $degree $commu_size $n1 $n2 $breaks 
printf "extract result fasta file\n"
printf "the extract fasta folder name is %s\n" $(pwd)
writefasta
#cd result
printf "the result folder name is %s\n" $(pwd)
newSize=$(du new.fasta | cut -f1)
##grep ">" new.fasta | cut -c2- > allnames
if [ $ovl -eq 1 ]
then
	printf "use ovl to find overlap\n"
	canu -correct -p "step2" maxThreads=8 -d "temp" genomeSize="$newSize" saveOverlaps=T corOutCoverage=400 corMinCoverage=0 Overlapper=ovl minReadLength=100 minOverlapLength=20 stopAfter=overlapStore -pacbio-corrected new.fasta
	if [ -e all.out ]
	then
		rm all.out
	fi
	for i in temp/correction/1-overlapper/001/*.ovb.gz
	do
	
		echo $i
		overlapConvert -G temp/correction/step2.gkpStore -coords ${i}  >> all.out ;
	done
	printf "use ovl.py to extract repeat reads\n"
	${DIR}/ovl.py all.out temp/correction/step2.ovlStore.per-read.log $replen $fromlen $replendiff $ratio $depth result
else
	printf "use mhap to find overlap\n"
	canu -correct -p "step2" maxThreads=8 -d "temp" genomeSize="$newSize" saveOverlaps=T corOutCoverage=400 corMinCoverage=0 minReadLength=100 minOverlapLength=20 stopAfter=overlapStore -pacbio-corrected new.fasta
	cat temp/correction/1-overlapper/results/*.mhap >  temp/correction/1-overlapper/results/all.mhap
	awk '{print $0,$7-$6,$11-$10 }' temp/correction/1-overlapper/results/all.mhap>all_new.mhap
	printf "use mhap.py to extract repeat reads\n"
	${DIR}/mhap.py all_new.mhap $replen $fromlen $replendiff $ratio $depth result
fi
printf "generate fasta from canu result\n"
cut -d " " -f1 result>linenum
cp temp/correction/step2.gkpStore/readNames.txt ./

while read line
do sed -n ${line}p readNames.txt | cut -f2 ; 
done < linenum > linenames

paste linenames <(cut -d " " -f2-3 result) > result.bed
sort result.bed | uniq > new.bed
faidx -b new.bed new.fasta > result.fasta
evaluation && rm -r temp && rm ../*.mhap && rm ../*.line
duration=$SECONDS
echo $folderName >> ${home}/combine.time
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed." >> ${home}/combine.time
#generate fasta from the read in new.fasta but not in the name of readNames.txt
#cut -f2 readNames.txt > canunames
#
#comm -23 <(sort allnames) <(sort mhapnames) > leftnames
#
#cat leftnames | xargs -n 1 samtools faidx new.fasta > part1.fasta
#
#cat part1.fasta part2.fasta >result.fasta
#
#



