#! /bin/bash
evaluation () {
#home=~/project/repeat
printf "start evaluation process\n"
bwa mem -x pacbio ${home}/dm3.fa -a result.fasta > result.sam
#number of multiple mapped read
repeatnum=$(samtools view -f 0x100  result.sam | cut -f1 | sort -u | wc -l)
#number of unmapped/mapped read
unmapped=$(samtools view  -f 4 result.sam |cut -f1 | sort -u | wc -l)
samtools view  -f 4 result.sam> unmapped.sam
samtools view -F 4 result.sam > mapped.sam
#repeatnum=$(cut -f1 mapped.sam| sort | uniq -c | sort -k1,1n | awk '$1>=2' | wc -l)
uniqnum=$(cut -f1 mapped.sam| sort | uniq -c | sort -k1,1n | awk '$1==1' | wc -l)
#repeat hisgram num#repeat hisgram num#repeat hisgram num
cut -f1 mapped.sam| sort | uniq -c | sort -k1,1n | awk '$1>=2' > RepeatHistNum
mapped=$(samtools view  -F 4 result.sam |cut -f1 | sort -u | wc -l)
#total read
total=$(grep -c ">" result.fasta)
samtools view -hb -f 0x100  result.sam | samtools sort - > repeat_sorted.bam
#cut -f1 repeat.sam | sort | uniq -c | sort -k1,1nr > repeat.num
blastn -query result.fasta -subject ${home}/drorep.ref -outfmt "6 qseqid sseqid pident qlen slen length mismatch gapopen qstart qend sstart send" > result.blast
cut -f2-6 result.blast  | sort -k1,1 -k5,5nr -k2,2nr |sort -u -k1,1 |awk '{print $0,$5/$3,$5/$4}' > BestResult.out
cut -f2-6 result.blast  |awk '{d=$3-$4;print $0,$5/$3,$5/$4,sqrt(d^2)}'| sort -k1,1 -k8,8n | sort -u -k1,1  > nearestResult.out
n10=$(awk '$7>0.1' BestResult.out |wc -l)
n50=$(awk '$7>0.5' BestResult.out |wc -l)
matchedNum=$(wc -l BlastResult.out| cut -d " " -f1)
totalNum=$(grep ">" ${home}/drorep.ref  | wc -l)
if [ -e num.out ]
then
	rm num.out
fi
printf "RepeatReads\t%d\n" $repeatnum | tee -a num.out
printf "UnmappedReads\t%d\n" $unmapped | tee -a num.out
printf "MappedReads\t%d\n" $mapped | tee -a num.out
printf "TotalReads\t%d\n" $total | tee -a num.out
printf "BlastMatchedNum\t%d\n" $matchedNum | tee -a num.out
printf "RefTotalNum\t%d\n" $totalNum | tee -a num.out
printf "0.1\t%d\n" $n10 | tee -a num.out
printf "0.5\t%d\n" $n50 | tee -a num.out
${DIR}/analysis.py result.sam
matchedRatio=$(tail -n 1  analysis.out | cut -f1)
indelRatio=$(tail -n 1 analysis.out | cut -f2)

printf "matchedBasesRatio\t%s\n" $matchedRatio | tee -a num.out
printf "indelBasesRatio\t%s\n" $indelRatio | tee -a num.out
bedtools bamtobed -i repeat_sorted.bam > repeat_sorted.bed 
bedtools coverage -a repeat_sorted.bed -b ${home}/dmel.polished.bed  > coverage.txt
cut -f7 coverage.txt > coverage.num
#RepeatMasker -pa 8 -dir .  -species drosophila result.fasta
#RepeatMasker -pa 8 -dir .  -lib result.fasta ${home}/drorep.ref
RepeatMasker -pa 8 -dir .  -lib result.fasta ${home}/dm3.fa
RepeatMasker -pa 8 -dir . -species drosophila dm3.fa.masked
genomeMasked=$(sed -n '6p' dm3.fa.tbl   | cut -d " " -f8)
genomeMaskedLeft=$(sed -n '6p' dm3.fa.masked.tbl   | cut -d " " -f8)
#resultMaskedPos=$(sed -n '6p' result.fasta.tbl | grep "%" -aob | grep -oE '[0-9]+')
#((resultMaskedPos-=6))
genomeMaskedPos=$(sed -n '6p' dm3.fa.tbl | grep "%" -aob | grep -oE '[0-9]+')
genomeMaskedLeftPos=$(sed -n '6p' dm3.fa.masked.tbl | grep "%" -aob | grep -oE '[0-9]+')
((genomeMaskedPos-=6))
((genomeMaskedLeftPos-=6))
#resultMaskedPos1=$(sed -n '6p' drorep.ref.tbl | grep "%" -aob | grep -oE '[0-9]+')
#((resultMaskedPos1-=6))
#resultMaskedString=$(sed -n '6p' result.fasta.tbl)
#resultMasked=${resultMaskedString:$resultMaskedPos:5}
genomeMaskedString=$(sed -n '6p' dm3.fa.tbl)
genomeMaskedLeftString=$(sed -n '6p' dm3.fa.masked.tbl)
genomeMaskedNum=${genomeMaskedString:$genomeMaskedPos:5}
genomeMaskedLeftNum=${genomeMaskedLeftString:$genomeMaskedLeftPos:5}
#resultMaskedString1=$(sed -n '6p' drorep.ref.tbl)
#resultMasked1=${resultMaskedString1:$resultMaskedPos1:5}
#genomeMasked=0
printf "genomeMasked\t%s\n" $genomeMaskedNum | tee -a num.out
printf "genomeMaskedLeft\t%s\n" $genomeMaskedLeftNum | tee -a num.out
#printf "resultMasked\t%s\n" $resultMasked | tee -a num.out
#printf "resultMasked1\t%s\n" $resultMasked1 | tee -a num.out
${DIR}/draw.R
paste <(echo $folderName)   <(cut -f2 num.out | awk -f ${DIR}/transpose.sh) >> $outputfile
}
