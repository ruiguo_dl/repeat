#!/usr/bin/python

import sys


overlapfile = sys.argv[1]
readlengthfile = sys.argv[2]
threshold1 = int(sys.argv[3])  #length threshold
threshold2 = int(sys.argv[4])  #from threshold
threshold3 = int(sys.argv[5])  #length difference threshold
threshold4 = float(sys.argv[6])  # repeat in hold read ratio threashold
threshold5 = int(sys.argv[7])  #depth  threshold

outputname = sys.argv[8]



#overlapfile = sys.argv[1]
#
#threshold1 = int(sys.argv[2])  #length threshold
#threshold2 = int(sys.argv[3])  #from threshold
#threshold3 = int(sys.argv[4])  #length difference threshold
#
#
#outputname = sys.argv[5]






def buildDict(filename,threshold1,threshold2,threshold3):


    readDict= {}
    repeatDict= {}


    with open(filename) as f:
        repeatidcount = 0
        for line in f:
            fields = line.split(" ")
            readid1 = fields[0]
            readid2 = fields[1]

            addreadone = True
            addreadtwo = True

            id1strand = int(fields[4])
            id1from = int(fields[5])
            id1to = int(fields[6])
            id1length = int(fields[7])

            id2strand = int(fields[8])
            id2from = int(fields[9])
            id2to = int(fields[10])
            id2length = int(fields[11])
            id1replength = int(fields[12])
            id2replength = int(fields[13])

            if abs(id1replength - id2replength) > threshold3:
                continue

#            if id1replength < (id1length * threshold4) or \
#                    id2replength < (id2length * threshold4):
#                continue

            cur1 = readDict

            if readid1 not in cur1:
                cur1[readid1] = {}
            cur1 = cur1[readid1]

            lengthrange1 = id1replength // threshold1 * threshold1
            lengthrange2 = id2replength // threshold1 * threshold1

            if lengthrange1 not in cur1:
                cur1[lengthrange1] = {}
            cur1 = cur1[lengthrange1]

            if id1strand not in cur1:
                cur1[id1strand] = {}
            cur1 = cur1[id1strand]

            fromrange = id1from // threshold2 * threshold2
            if fromrange not in cur1:
                cur1[fromrange] = {}
            else:
                addreadone = False

            cur1 = cur1[fromrange]

            cur2 = readDict

            if readid2 not in cur2:
                cur2[readid2] = {}
            cur2 = cur2[readid2]

            if lengthrange2 not in cur2:
                cur2[lengthrange2] = {}
            cur2 = cur2[lengthrange2]

            if id2strand not in cur2:
                cur2[id2strand] = {}
            cur2 = cur2[id2strand]

            fromrange = id2from // threshold2 * threshold2
            if fromrange not in cur2:
                cur2[fromrange] = {}
            else:
                addreadtwo = False

            cur2 = cur2[fromrange]

            readid = readid1 if id1strand == 0 else readid2
            idfrom = id1from if id1strand == 0 else id2from
            idto = id1to if id1strand == 0 else id2to
            idstrand = id1strand if id1strand == 0 else id2strand
            idrange = lengthrange1 if id1strand == 0 else lengthrange2

            if addreadone and addreadtwo:
                cur3 = repeatDict
                repeatidcount += 1
                cur1['repeatid'] = repeatidcount
                cur2['repeatid'] = repeatidcount
                cur3[repeatidcount] = {}

                cur3 = cur3[repeatidcount]
                cur3['depth'] = 1
                cur3['pointer'] = None
                cur3['read'] = [readid, idfrom, idto, idrange, idstrand]

            elif addreadone and not addreadtwo:
                repeatid = cur2['repeatid']

                while repeatDict[repeatid]['pointer']:
                    repeatid = repeatDict[repeatid]['pointer']

                cur1['repeatid'] = repeatid
                repeatDict[repeatid]['depth'] += 1
                if repeatDict[repeatid]['read'][3] < idrange:
                    repeatDict[repeatid]['read'] = [readid, idfrom, idto, idrange, idstrand]

            elif not addreadone and addreadtwo:
                repeatid = cur1['repeatid']

                while repeatDict[repeatid]['pointer']:
                    repeatid = repeatDict[repeatid]['pointer']

                cur2['repeatid'] = repeatid
                repeatDict[repeatid]['depth'] += 1
                if repeatDict[repeatid]['read'][3] < idrange:
                    repeatDict[repeatid]['read'] = [readid, idfrom, idto, idrange, idstrand]


            else:
                repeatid1 = cur1['repeatid']
                repeatid2 = cur2['repeatid']

                while repeatDict[repeatid1]['pointer']:
                    repeatid1 = repeatDict[repeatid1]['pointer']

                while repeatDict[repeatid2]['pointer']:
                    repeatid2 = repeatDict[repeatid2]['pointer']

                if repeatid1 != repeatid2:
                    repeatDict[repeatid2]['pointer'] = repeatid1
                    depth = repeatDict[repeatid2]['depth']
                    repeatDict[repeatid1]['depth'] += depth
                else:
                    repeatDict[repeatid1]['depth'] += 1

                if repeatDict[repeatid1]['read'][3] < idrange:
                    repeatDict[repeatid1]['read'] = [readid, idfrom, idto, idrange, idstrand]
        return repeatDict



def buildDictOvt(overlapfile,readlengthfile,threshold1,threshold2,threshold3, threshold4):


    readDict= {}
    repeatDict= {}
    lengthDict = {}

    with open(overlapfile,'r') as a, open(readlengthfile,'r') as b:
        for line in b:
            fields = line.split()
            readid = fields[0]
            readlength = fields[1]
            lengthDict[readid] = int(readlength)

        repeatidcount = 0

        for line in a:
            fields = line.split()
            readid1 = fields[0]
            readid2 = fields[1]

            replength=int(fields[3])

            id1from = int(fields[4])
            id1to = int(fields[5])

            id2from = int(fields[6])
            id2to = int(fields[7])

            id1length = lengthDict[readid1]
            id2length = lengthDict[readid2]

            addreadone = True
            addreadtwo = True

            id1strand = 0

            id2strand = 1 if id2from >= id2to else 0


            # if abs(id1replength - id2replength)> threshold3:
            #     continue

            # length difference should be less than threshold3
            if abs(id1length - id2length) > threshold3:
                continue

            #replength should not be less than a ratio of the total read length
            if replength < (id1length * threshold4) or \
                    replength < (id2length * threshold4):
                continue

            cur1 = readDict

            if readid1 not in cur1:
                cur1[readid1] = {}
            cur1 = cur1[readid1]

            lengthrange = replength // threshold1 * threshold1

            if lengthrange not in cur1:
                cur1[lengthrange] = {}
            cur1 = cur1[lengthrange]

            if id1strand not in cur1:
                cur1[id1strand] = {}
            cur1 = cur1[id1strand]

            fromrange = id1from // threshold2 * threshold2
            if fromrange not in cur1:
                cur1[fromrange] = {}
            else:
                addreadone = False

            cur1 = cur1[fromrange]

            cur2 = readDict

            if readid2 not in cur2:
                cur2[readid2] = {}
            cur2 = cur2[readid2]

            if lengthrange not in cur2:
                cur2[lengthrange] = {}
            cur2 = cur2[lengthrange]

            if id2strand not in cur2:
                cur2[id2strand] = {}
            cur2 = cur2[id2strand]

            fromrange = id2from // threshold2 * threshold2
            if fromrange not in cur2:
                cur2[fromrange] = {}
            else:
                addreadtwo = False

            cur2 = cur2[fromrange]

            readid = readid1 if id1strand == 0 else readid2
            idfrom = id1from if id1strand == 0 else id2from
            idto = id1to if id1strand == 0 else id2to
            idstrand = id1strand if id1strand == 0 else id2strand
            idrange = lengthrange if id1strand == 0 else lengthrange

            if addreadone and addreadtwo:
                cur3 = repeatDict
                repeatidcount += 1
                cur1['repeatid'] = repeatidcount
                cur2['repeatid'] = repeatidcount
                cur3[repeatidcount] = {}

                cur3 = cur3[repeatidcount]
                cur3['depth'] = 1
                cur3['pointer'] = None
                cur3['read'] = [readid, idfrom, idto, idrange, idstrand]

            elif addreadone and not addreadtwo:
                repeatid = cur2['repeatid']

                while repeatDict[repeatid]['pointer']:
                    repeatid = repeatDict[repeatid]['pointer']

                cur1['repeatid'] = repeatid
                repeatDict[repeatid]['depth'] += 1
                if repeatDict[repeatid]['read'][3] < idrange:
                    repeatDict[repeatid]['read'] = [readid, idfrom, idto, idrange, idstrand]

            elif not addreadone and addreadtwo:
                repeatid = cur1['repeatid']

                while repeatDict[repeatid]['pointer']:
                    repeatid = repeatDict[repeatid]['pointer']

                cur2['repeatid'] = repeatid
                repeatDict[repeatid]['depth'] += 1
                if repeatDict[repeatid]['read'][3] < idrange:
                    repeatDict[repeatid]['read'] = [readid, idfrom, idto, idrange, idstrand]


            else:
                repeatid1 = cur1['repeatid']
                repeatid2 = cur2['repeatid']

                while repeatDict[repeatid1]['pointer']:
                    repeatid1 = repeatDict[repeatid1]['pointer']

                while repeatDict[repeatid2]['pointer']:
                    repeatid2 = repeatDict[repeatid2]['pointer']

                if repeatid1 != repeatid2:
                    repeatDict[repeatid2]['pointer'] = repeatid1
                    depth = repeatDict[repeatid2]['depth']
                    repeatDict[repeatid1]['depth'] += depth
                else:
                    repeatDict[repeatid1]['depth'] += 1

                if repeatDict[repeatid1]['read'][3] < idrange:
                    repeatDict[repeatid1]['read'] = [readid, idfrom, idto, idrange, idstrand]
        return repeatDict


#repeatDict = buildDict(overlapfile,threshold1,threshold2,threshold3)
repeatDict = buildDictOvt(overlapfile,readlengthfile,threshold1,threshold2, threshold3,threshold4)
#repeatDict = buildDictOvt('all.out','readlength',100,50,100)
# outputname='result'
# threshold3=3

with open(outputname, 'w') as f1:
    for key in repeatDict:
        cur = repeatDict[key]
        if not cur['pointer']:
            if cur['depth'] > threshold5:
                readid = cur['read'][0]
                readfrom = str(cur['read'][1])
                readto = str(cur['read'][2])
                readlength = str(cur['read'][3])
                readstrand = str(cur['read'][4])
                depth = str(cur['depth'])
                f1.write(readid + ' ' + readfrom + ' ' +
                         readto + ' ' + readlength + ' ' + readstrand + ' ' +
                         depth + '\n')

sys.exit()








